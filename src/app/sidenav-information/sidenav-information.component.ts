import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sidenav-information',
  templateUrl: './sidenav-information.component.html',
  styleUrls: ['./sidenav-information.component.scss']
})
export class SidenavInformationComponent implements OnInit {
  showFiller = false;

  test = false;
  
  constructor() { }
  
  ngOnInit() {
  }

  onOff( ) :void{
    this.test = !this.test;
  }

}
