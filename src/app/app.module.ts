import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatToolbarModule, MatIconModule, MatSidenavModule, MatTreeModule } from '@angular/material';
import { ToolbarMenuComponent } from './toolbar-menu/toolbar-menu.component';
import { SidenavInformationComponent } from './sidenav-information/sidenav-information.component';
import { TreeSidenavComponent } from './tree-sidenav/tree-sidenav.component';
import {MatTooltipModule} from '@angular/material/tooltip';




@NgModule({
  declarations: [
    AppComponent,
    ToolbarMenuComponent,
    SidenavInformationComponent,
    TreeSidenavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatTreeModule,
    MatTooltipModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
